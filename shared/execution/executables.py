"""
module
"""

from __future__ import annotations

from abc import ABC, abstractmethod

# -----------------------------------------------------------------------------
# INTERFACES
# -----------------------------------------------------------------------------


class IExecutable(ABC):
    """
    IExecutable
    """

    @abstractmethod
    def execute(self) -> None:
        """
        method
        """
