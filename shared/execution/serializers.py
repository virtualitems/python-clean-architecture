"""
module
"""

from __future__ import annotations
from typing import Any

from abc import ABC, abstractmethod

# -----------------------------------------------------------------------------
# INTERFACES
# -----------------------------------------------------------------------------


class ISerializable(ABC):
    """
    ISerializable
    """

    @abstractmethod
    def serialize(self) -> Any:
        """
        serialize
        """
