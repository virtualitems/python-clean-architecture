"""
module
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass

# -----------------------------------------------------------------------------
# ABSTRACT CLASSES
# -----------------------------------------------------------------------------


@dataclass
class UseCaseRequest(ABC):
    """
    UseCaseRequest
    """


@dataclass
class UseCaseResponse(ABC):
    """
    UseCaseRequest
    """


class UseCase(ABC):
    """
    UseCase
    """

    @abstractmethod
    def handle(self, request: UseCaseRequest) -> UseCaseResponse:
        """
        handle
        """
