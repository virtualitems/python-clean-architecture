"""
module
"""

from __future__ import annotations

from abc import ABC, abstractmethod
from collections.abc import Collection
from dataclasses import dataclass

from shared.domain.entities import Entity


# -----------------------------------------------------------------------------
# ABSTRACT CLASSES
# -----------------------------------------------------------------------------

@dataclass
class Query(ABC):
    """
    Query
    """


@dataclass
class Result(ABC, Collection):
    """
    Result
    """


class Repository(ABC):
    """
    Repository
    """
    @abstractmethod
    def all(self, query: Query) -> Result:
        """
        all
        """

    @abstractmethod
    def none(self) -> Result:
        """
        none
        """

# -----------------------------------------------------------------------------
# INTERFACES
# -----------------------------------------------------------------------------


class IFilterable(ABC):
    """
    Filterable
    """

    @abstractmethod
    def filter(self, query: Query) -> Result:
        """
        filter
        """

    @abstractmethod
    def get(self, query: Query) -> Entity:
        """
        get
        """


class ICreatable(ABC):
    """
    Creatable
    """

    @abstractmethod
    def create(self, entity: Entity) -> None:
        """
        create
        """


class IUpdatable(ABC):
    """
    Updateable
    """

    @abstractmethod
    def update(self, entity: Entity) -> None:
        """
        update
        """


class IDeletable(ABC):
    """
    Deletable
    """

    @abstractmethod
    def delete(self, entity: Entity) -> None:
        """
        delete
        """


# -----------------------------------------------------------------------------
# EXCEPTIONS
# -----------------------------------------------------------------------------


class RepositoryException(Exception):
    """
    RepositoryException
    """


class ObjectDoesNotExist(RepositoryException):
    """
    ObjectDoesNotExist
    """


class MultipleObjectsReturned(RepositoryException):
    """
    MultipleObjectsReturned
    """


class ObjectAlreadyExists(RepositoryException):
    """
    ObjectAlreadyExists
    """


class OperationFailure(RepositoryException):
    """
    OperationFailure
    """
