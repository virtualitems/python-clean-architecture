"""
module
"""

from __future__ import annotations

from abc import ABC
from dataclasses import dataclass

# -----------------------------------------------------------------------------
# ABSTRACT CLASSES
# -----------------------------------------------------------------------------


@dataclass
class Entity(ABC):
    """
    dataclass
    """
